# -*- coding: utf-8 -*-
import os
import tempfile

import datetime

from config import CSS_PATH, WKHTMLTOPDF_PATH
from models import Student, Order, order_types
from flask import render_template, send_file
import pdfkit

order_info = {
    'date': datetime.datetime.now(),
    'number': u'№ 285-с',
    'rector': u'В. В. Грабко'
}
reason = u'заяви, одруження, тири-пири, тралі-валі і ще деякий текст, потрібний для довжини цього рядка.'


def order_academ_ended():
    return render_template('academ_ended.html', order_info=order_info, reason=reason,
                           students=Student.objects.all())


def order_academ_go_on():
    return render_template('academ_go_on.html', order_info=order_info, reason=reason,
                           students=Student.objects.all())


def order_academ_give():
    students = Student.objects
    css = os.path.join(CSS_PATH, 'print.css')
    html_file = render_template('academ_give.html', order_info=order_info, reason=reason,
                                students=students)
    config = pdfkit.configuration(wkhtmltopdf=WKHTMLTOPDF_PATH)

    with tempfile.NamedTemporaryFile(delete=False, suffix='.pdf') as f:
        pdfkit.from_string(html_file, f.name,
                           configuration=config,
                           options={'encoding': "UTF-8",
                                    'margin-top': '1cm',
                                    'margin-right': '1cm',
                                    'margin-bottom': '1.2cm',
                                    'margin-left': '2cm',
                                    'title': u'order',
                                    'footer-right': '[page]',
                                    # 'footer-left': u'Generated by Yaroslav',
                                    'footer-font-size': 9,
                                    },
                           css=css)
    pdf = open(f.name, 'rb')

    # створення наказу й додання до нього пдф-ки
    order = Order(number=order_info['number'],
                  date=order_info['date'],
                  order_type=order_types[3][0])

    for st in students:
        order.students.append(st)

    order.file.new_file()
    order.file.replace(pdf)
    order.save()

    sent_file = send_file(pdf)
    return sent_file

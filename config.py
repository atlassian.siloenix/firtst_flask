import os

import sys

from utils import find_file

APP_ROOT = os.path.dirname(os.path.abspath(__file__))
VIRTUALENV_BIN = os.path.dirname(sys.executable)
VIRTUALENV_ROOT = VIRTUALENV_BIN.rstrip('bin')
CSS_PATH = os.path.join(APP_ROOT, 'static', 'css')
WKHTMLTOPDF_PATH = os.path.join('/', 'usr', 'bin', 'wkhtmltopdf')

print 'wkhtmltopdf path: ', WKHTMLTOPDF_PATH
print CSS_PATH

# -*- coding: utf-8 -*-
from mongoengine import Document, IntField, StringField, FileField, DateTimeField, ListField, ReferenceField


class Student(Document):
    full_name = StringField(max_length=120)
    full_name_d = StringField(max_length=120)
    full_name_r = StringField(max_length=120)
    sex = StringField(max_length=30)

    course = IntField()
    degree = StringField(max_length=20)
    termin = StringField(max_length=40)
    form_time = StringField(max_length=20)
    form_type = StringField(max_length=20)
    form_time_r = StringField(max_length=20)
    form_type_r = StringField(max_length=20)
    faculty = StringField(max_length=20)
    specialty = StringField(max_length=30)
    group = StringField(max_length=20)

    def __unicode__(self):
        return unicode(self.full_name)


order_types = (
    (u'Відрахування', u'Відрахування'),
    (u'Переведення на курс', u'Переведення на курс'),
    (u'Поновлення', u'Поновлення'),
    (u'Відправлення в академічну відпустку', u'Відправлення в академічну відпустку'),
    (u'Повернення з академічної відпустки', u'Повернення з академічної відпустки'),
    (u'Продовження академічної відпустки', u'Продовження академічної відпустки')
)


class Order(Document):
    number = StringField(required=True)
    date = DateTimeField(required=True)
    order_type = StringField(required=True, choices=order_types)
    students = ListField(ReferenceField(document_type='Student'))
    file = FileField()


def first_setting():
    Student(full_name=u'Драченко Ярослав Петрович',
            full_name_r=u'Драченка Ярослава Петровича',
            full_name_d=u'Драченку Ярославу Петровичу',
            sex=u'чоловіча', course=2,
            degree=u'бакалавр', termin=u'',
            form_time=u'денна', form_type=u'держбюджетна',
            form_time_r=u'денної', form_type_r=u'держбюджетної', faculty=u'ФІТКІ',
            specialty=u'6.050202', group=u'1ПІ-15б').save()

    Student(full_name=u'Коржук Василина Петрівна',
            full_name_r=u'Коржук Василину Петрівну',
            full_name_d=u'Коржук Василині Петрівні',
            sex=u'жіноча',course=2,
            degree=u'бакалавр', termin=u' (зі скороченим терміном навчання)',
            form_time=u'денна', form_type=u'держбюджетна',
            form_time_r=u'денної', form_type_r=u'держбюджетної', faculty=u'ФІТКІ',
            specialty=u'6.050202', group=u'1ПІ-15б').save()


